import { expect } from "chai";
import { checkStatusCode, checkResponseBodyStatus, checkResponseBodyMessage, checkResponseTime } from "../../helpers/functionsForCheking.helper";
import { AboutAuthorGet, LoginControlller, AboutAuthorSet, UserMe, Overview, PostArticle, GetArticle, GetArticleById, PostArticleComment, GetArticleComment } from "../lib/controllers/auth_controller";


var chai = require('chai');
chai.use(require('chai-json-schema'));
const schemas = require('./data/schemas_testData.json');


const login = new LoginControlller();
const aboutget = new AboutAuthorGet();
const about = new AboutAuthorSet();
const userme = new UserMe();
const overview = new Overview();
const postarticle = new PostArticle();
const getarticle = new GetArticle();
const getarticlebyid = new GetArticleById();
const postarticlecomment = new PostArticleComment();
const getarticlecomment = new GetArticleComment();



describe("Author sequence refactored", () => {;
    let email: string = 'cssiayrrxglqixzief@kvhrs.com';
    let password: string = 'Shrek123';
    let token;
    let id;
    let userId;
    let articleId;


    it(`Check environment`, async () => {
        console.log(`Now we are using the ${global.appConfig.envName}`);
    });

    describe('Using test data for login', () => {
        let invalidCredentialsDataSet = [
            { email: 'qtilxlcvtrgtzqnthu@nthrw.com', password: '      ' },
            { email: 'qtilxlcvtrgtzqnthu@nthrw.com', password: 'Shrek123 ' },
            { email: 'qtilxlcvtrgtzqnthu@nthrw.com', password: 'Shre 123k' },
            { email: 'qtilxlcvtrgtzqnthu@nthrw.com', password: 'kekwlmao' },
            { email: 'qtilxlcvtrgtzqnthu@nthrw.com', password: 'qtilxlcvtrgtzqnthu@nthrw.com' },
            { email: 'qtilxlcvtrgtzqnthu@nthrw.com ', password: 'Shrek123' },
            { email: 'qtilxlcvtrgtzqnthu@nthrw.com  ', password: 'kappa' },
        ];
    
        invalidCredentialsDataSet.forEach((credentials) => {
            it(`login using invalid credentials : ${credentials.email} + ${credentials.password}`, async () => {
                let response = await login.login(credentials.email, credentials.password);
    
                checkStatusCode(response, 401);
                checkResponseBodyStatus(response, 'UNAUTHORIZED');
                checkResponseBodyMessage(response, 'Bad credentials');
                checkResponseTime(response, 3000);
            });
        });
    });

    
    before('Get access token and id + userId', async () => {
        let response = await login.login(email, password);

        // console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        //expect(response.body).to.be.jsonSchema(schemas.schema_login);

        token = response.body.accessToken;


        response = await aboutget.Myinfo(token);

        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_myinfo);

        id = response.body.id;
        userId = response.body.userId;                                       
    });


    // SET INFO


    it('Setinfo', async () => {
        let response = await about.setinfo(token, id, userId);

        //console.log(response.body);

        //checkStatusCode(response, 200);                                       // commented out for safety purposes
        //checkResponseTime(response, 5000);
        //expect(response.body).to.be.jsonSchema(schemas.schema_setinfo);
    });



    describe('Trying to get info with the wrong token', () => {
        let invalidTokens = [
            { token: 'invalidtoken' },
            { token: 'kekw' },
            { token: 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJiM2YyZGNkZi1hNzA5LTQyNzktYjc1MC1hNmFiMWEyMDRiOGIiLCJpYXQiOjE2NTg5OTUwMjQsImV4cCI6MTY1OTA4MTQyNH0.i7y_U1RKwFOLyvlzCdnfpQhJNH9f94AKM2-mpMiuGU6d6s7dKmYKwiXzTe76Tt2p_LM2PTIET-yVEIz8Gxyzw' },
            { token: '               ' },
            { token: " i ain't a token " },
        ];
    
        invalidTokens.forEach((tokens) => {
            it(`Getting info with wrong token : ${tokens.token}`, async () => {
                let response = await userme.userme(tokens);;
    
                //checkStatusCode(response, 401);
                checkResponseBodyStatus(response, 401);
                checkResponseBodyMessage(response, '');
                checkResponseTime(response, 3000);
            });
        });
    });



    it('Userme',async () => {
        let response = await userme.userme(token);

        //console.log(response.body);
        
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_userme);
    });



    it('Overview',async () => {
        let response = await overview.overview(token, id);

        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_overview);
    });



    it('Post an article',async () => {
        let response = await postarticle.postarticle(token);

        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_postarticle);
    });


    it('Get info about article',async () => {
        let response = await getarticle.getarticle(token);

        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_getinfoarticle);


        articleId = response.body[0].id;
    });


    describe('Trying to get info about article with the wrong ID', () => {
        let invalidIds = [
            { id: 'My end' },
            { id: 'It justifies my means' },
            { id: 'All I have to do is delay' },
            { id: "I'm given time to evade" },
            { id: "The end of the road is my end" },
            { id: '           ' },
        ];
    
        invalidIds.forEach((idinarray) => {
            it(`Getting info with wrong ID : ${idinarray.id}`, async () => {
                let response = await getarticlebyid.getarticlebyid(token, id);
    
                checkStatusCode(response, 404);
                checkResponseBodyStatus(response, 'NOT_FOUND');
                checkResponseBodyMessage(response, 'No value present');
                checkResponseTime(response, 3000);
            });
        });
    });


    it('Get info about article by ID',async () => {
        let response = await getarticlebyid.getarticlebyid(token, articleId);

        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_getarticlebyid);
    });


    it('Post article comment',async () => {
        let response = await postarticlecomment.postarticlecomment(token, articleId);

        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_postarticlecomment);
    });



    it('Get article comment',async () => {
        let response = await getarticlecomment.getarticlecomment(token, articleId);

        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_getarticlecomment);
    });


    afterEach(function () {
        console.log('Testing after each hook - author seq');
    });
});